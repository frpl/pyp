# parsetest.py: Test routine for parsers.  From frplparse.py.
# Copyright (c) 2013--2015 cxw

def test(elem, teststr, expected, printparse=False, vars=None):
    """Main test function.
        PyParsing _elem_ either does or does not match _teststr_.  If that
        comparison is opposite from bool _expected_, print a message. """

    # Get the context
    lineno = getframeinfo(currentframe().f_back).lineno
    theline=str(lineno)+':'

    # In general, we can only run the string once, so run it here and
    # save any exception.
    matched = True
    theparse = None
    theexception = None
    try:
        theparse = elem.parseString(teststr)
    except ParseBaseException as err:
        matched = False
        theexception = err
    #end try

    if matched!=expected:
        # print caller's line number and a debug message
        print(theline)
        stdout.flush()
        print( (elem.name if hasattr(elem,"name") else "unnamed element"),
                "failed on --" + teststr + "--: should have",
                ("matched" if expected else "missed"), "but didn't.")

        if not matched: # if we wanted it to but it didn't,
                        # show the exception.
            print('  -> exception was %s.'%theexception)

    if printparse and not DOTEST.SILENT:  # whether or not it matched.
        if matched:
            print('%s -- %s -- parsed to == %s =='%(theline, teststr,
                            str(theparse)))
        else:
            print('%s -- %s -- parse failed'%(theline, teststr))
        stdout.flush()

    # end test

# History:
#   2015/12/20  cxw     Initial version

# vi: set ts=4 sts=4 sw=4 expandtab ai: #

