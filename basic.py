# basic.py: Basic test of infixNotation vs. IELL
# Copyright (c) 2015 cxw

import pdb
from pyp import *
from parsetest import test

operand = Word(nums)

inexpr = infixNotation( 
    baseExpr=operand,
    lpar='(', rpar=')', name='in',
    opList=[
            ('::', 1, opAssoc.RIGHT),
            ('--', 1, opAssoc.RIGHT),   # prefix
            ('--', 1, opAssoc.LEFT),    # postfix
            ('*', 2, opAssoc.LEFT),
            ('+', 2, opAssoc.LEFT),
           ]
    ) #inexpr

llexpr = IELL(
    operandExpr = operand,
    tokList=['::','--','*','+'],
    opList=[
        ('::', 1, Infix.PREFIX),
        ('--', 1, Infix.PREFIX),   # prefix
        ('--', 1, Infix.POSTFIX),    # postfix
        ('*', 2, Infix.LEFT),
        ('+', 2, Infix.LEFT),
    ]
    ) #llexpr

inret = inexpr.parseString('--::1*2-- + 3')
#print(type(inret))
print(inret)
llret = llexpr.parseString('--::1*2-- + 3')
#print(type(llret))
print(llret)
print(str(inret)==str(llret))
print(repr(inret)==repr(llret))

try:
    llexpr.parseString('foo')
except Exception as e:
    print('Exception '+str(e))

ll2 = llexpr + llexpr
ll2ret = ll2.parseString('--5 ::6')
#print(type(ll2ret))
print(ll2ret)

#test(inexpr,'--::1*2-- + 3', True, True)

# History:
#   2015/12/20  cxw     Initial version

# vi: set ts=4 sts=4 sw=4 expandtab ai: #
