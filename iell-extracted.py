# iell-extracted.py: Extracted for ease of review.
# *** DO NOT EDIT ***
# Make any changes in pyp.py.

# Changes from pyparsing 2.0.0 are Copyright (c) 2012-2013 Chris White.
# The following license applies to those changes:
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# Future enhancements to IELL:
# 1. Figure out how to add pre-token or post-token
#    support to operators.  Right now, e.g., a ternary
#    expression with operators X and Y can only be of the
#    form op1 X op2 Y op3.  Permit, e.g., X op1 Y op2 Z op3
#    or op1 X op2 Y op3 Z, or ops on both ends.  This could
#    also be useful for a unary operator.  For example,
#    you could define a unary lift:
#       liftexpr ::= 'lift' '<' <op> '>' 
#    You could also use it for C++ typecasts:
#       castexpr ::= 'dynamic_cast' '<' op1 '>' '(' op2 ')'
# 2. See if Adj can be permitted in an op with arity>2, either as the first
#    operator (e.g., "a b:2") or later (e.g., "a?b c").
# 3. For now, a token can't be used, e.g., both as a binary operator and as 
#    part of a ternary operator.  See if this restriction can be relaxed.
#    This affects thismap[HASPOST] in IELL._loadoperator.
# 4. At present, each operator is assigned a name by IELL.__init__, and the
#    text of the operator is returned rather than its name.  Should user-
#    provided operator names be returned instead?  Should a node with
#    named keys (.op, .lhs, .rhs) be returned instead of an iterable?
#    Should prefix and postfix operators be differentiated by something other
#    than the current None flag value?
# 5. Figure out if there is a nicer way to differentiate consecutive
#    expressions than the tuple-wrap presently used.  
#    Affects IELL.parseImpl.
# 6. Expand the nextarity==3 case in IELL._parse to loop for any arity >=3.
# 7. Determine whether the postfix-operator loop in IELL._parse could be
#    rolled into the infix-expression loop it immediately precedes.
# 8. In IELL.__init__, descend each tokExpr to verify that FollowedBy and
#    Optional are not used.

# Special values for IELL
Infix = _Constants()
Infix.POSTFIX = Infix.LEFT = NamedConstant("Lassoc/postfix")
Infix.PREFIX = Infix.RIGHT = NamedConstant("Rassoc/prefix")
AdjOp = NamedConstant("Adj")
    # Placeholder returned by the Adj operator instead of ''.
    # This is so the parse results will read more cleanly.

# Token names
TokName = _Constants()
TokName.OPEN = NamedConstant("tok.open")
TokName.CLOSE = NamedConstant("tok.close")
TokName.ADJ = NamedConstant("tok.Adj")
    # a fake binary operator for expressions that permit putting one
    # operator immediately after another (e.g., string concat in C or awk)

# Helper for IELL
def _stuffAdjOp(s, l, toks):
# The Adj operator uses an Empty().  As a result, we need to put in some
# token code the caller can test.  Infix.ADJOP is guaranteed to be a
# unique hash key, so use it.
    toks.insert(0, AdjOp)
#end _stuffAdjOp()

class IELL(ParserElement): #IELL.  Short name during debugging.
    """Parse an infix expression using LL(k) techniques from
        Hanson and Fraser, ISBN 0-8053-1760-1, expanded
        by cxw.  Related to infixNotation(), but not a drop-in replacement.
        Returns a prefix tree of the parsed expression.
        Once an instance is constructed, treat it as immutable.
        This class is not designed to correctly handle any outside changes to
        its internal data after __init__()."""

    # constants: attributes of tokens
    _taPREC = NamedConstant("Prec")         # name for the precedence saved by the tokenizer
    _taNAME = NamedConstant("Name")         # name for the precedence name itself
    # Special token names
    _tnEOI = NamedConstant("tok.EOI")           # name for end of the input
    _tnOPERAND = NamedConstant("tok.Operand")   # name for an operand

    # Operator names
    _onEOI = NamedConstant("op.EOI")
    _onOPERAND = NamedConstant("op.Operand")
    _onOPEN = NamedConstant("op.OPEN")
    _onCLOSE = NamedConstant("op.CLOSE")
    _onADJ = NamedConstant("op.ADJ")
    _onMISPLACED = NamedConstant("op.MISPLACED")
        # Special operator, returned when we saw an operator in a context
        # we didn't expect it.

    # precedence levels
    _precDONE=0           # done parsing this expression
    _precOPERAND=1        # saw what might be an operand.
    _precDELIM=2          # parens
    _precBOTTOM=3         # lowest precedence level of an operator

    # Mapping assocL to pre/postfix
    _ASSOCL_POSTFIX = True     # postfix is left-associative
    _ASSOCL_PREFIX = False     # prefix is right-associative,
                                # which is False in self._assocL

    # Parsing contexts
    _ctxNONE = NamedConstant('NoContext')
        # special value for, e.g., parens that should be in no context.
    _ctxPRE = -1   # special value for prefix ops
    _ctxOP0 = 0    # _ctxOP0 + i is the i'th operator, starting from
                    # i=0 for a postfix op.

    # Error-message helpers
    def _c2s(self, ctx):
        if ctx==self._ctxPRE:
            return "prefix"
        elif ctx==self._ctxOP0:
            return "postfix or binary"
        else:
            return "multi-arity position {}".format(ctx-self._ctxOP0+1)
    #end _c2s

    # constructor
    def __init__(self, operandExpr, 
                 tokList, opList, savelist=False):
        """ operandExpr: a ParserElement that will match
                an operand.  No further subdivision is performed.
            tokList: tokenizer information.
                This is a list of elements, each of which is a string
                or a tuple (tokName, tokExpr).  Passing just 'foo' is
                equivalent to passing ('foo','foo').
                tokName is a name of the token.  E.g., "addop".
                    Token names can be anything that can serve as a
                    dict() key.  The same token name can be used on
                    multiple rows of tokList if two different sequences of 
                    characters represent the same token (e.g., && and "and"
                    in C++).  
                tokExpr is a ParserElement or string that will match
                    that token.  Do not use "FollowedBy"
                    or "Optional" in tokExpr.  E.g., oneOf("+ -")
                    Parse actions in tokExprs are always performed.
                Tokens are detected by scanning from beginning to end
                of tokList.  Therefore, e.g., put "++"
                before "+", or else "+" will match and
                ++ will never be seen.  Operands are checked after tokList.

                Do include the open and close delimiters in the list;
                use TokName.OPEN and TokName.CLOSE as the names.
                The defaults are "(",TokName.OPEN and ")",TokName.CLOSE.
                If you do not include them, they will be checked before
                all the expressions on the list.

                The tokenizer checks for operands after the tokens you list.

            opList: operators.
                This is a list of tuples, from highest precendence to lowest.
                Do not include operands or open/close delimiters.
                Each tuple is: (tokName, numTerms, RLAssoc[, opParseAction]).
                tokName: an item or tuple of tokName values from tokList.  
                  - The corresponding tokExpr(s) is/are the parse 
                    expression(s) for the operator.
                  - If you want:
                        expr ::= <operand> <operand>
                    to work (e.g., like string concatenation in C or awk:
                    "x" "y" => "xy"), specify TokName.ADJ as the tokName.
                  - The same tokName can be used for prefix and infix, or
                    for prefix and postfix, but not for postfix and infix.
                  - If you are using an ADJ operator anywhere in the grammar,
                    you cannot use the same token for both prefix and postfix.
                    (Doing so would create an ambiguous grammar.)
                  - Elements of a tokName tuple must be unique.
                numTerms: the arity of the operator, integer >=1.
                  - If numTerms >= 3, the number of items in tokName must be
                    numTerms-1, and the items define the operators between
                    the operands.  For example, 
                        ( ('?',':'), 3, Infix.LEFT )
                    matches expressions like "a?b:c".
                  - If numTerms < 3, each item in tokName is an operator
                    of the same characteristics.  For example,
                        ( ('+', '-'), 2, Infix.LEFT )
                    matches "a+b-c", with + and - having the same precedence.
                RLAssoc: Infix.LEFT or Infix.RIGHT.  For unary
                    operators, use Infix.POSTFIX and Infix.PREFIX.
                    Postfix operators are always left-associative and
                    prefix operators are always right-associative.
                opParseAction: optional parse action called when this operator
                    is matched.  The opParseAction is called as:
                        opParseAction(str, loc, tree)
                    where _loc_ is where the matching of the WHOLE EXPRESSION
                    started (NOT this subexpression, because subexpressions
                    can be quite complex), and _tree_ is a ParseResults-
                    packaged prefix tree of the result at this point.  
                    If opParseAction returns anything other than None, 
                    _tree_ is replaced with that return value.
        """
        # Init vars. =======================================================
        super().__init__(savelist)
        self.setName("IELL")

        # Save the operand parameters
        self._operandExpr = operandExpr

        # Initialize variables
        self._tn2on = {}
            # nested map TokName -> ParsingContext -> OperatorName
        self._prec = {}  # OperatorName -> integer precedence
        self._maxoperatorprec = self._precBOTTOM + (len(opList)-1)
        self._maxprec = self._maxoperatorprec+1
            # first operator level, then remaining operator levels,
            # then open paren at the top
        self._subops = {}
            # For arity>=3, maps first opname to iterable of subsequent opnames.
        self._opParseActions={}
            # map OperatorName->ParseAction.
            # This is separate from the inherited self.parseAction because
            # these are called as the tree is built.  The regular 
            # addParseAction functions are called on the complete tree.
            # Only one opParseAction is provided for each operator, since
            # there is only one slot on the corresponding row.

        # Process the tokenizer. ===========================================
        # Make a MatchFirst as the tokenizer for operators
        toks = []   # the list of ParseElements we're building
        sawOpen = sawClose = False

        for i,tokDef in enumerate(tokList):
            if isinstance(tokDef,basestring):
                tokName = tokExpr = tokDef
            else:
                tokName, tokExpr = tokDef[:]
            #endif string passed
            thisexpr = self._makeTokenExpr(tokName, tokExpr)
            toks.append(thisexpr)
            if tokName==TokName.OPEN: sawOpen = True
            if tokName==TokName.CLOSE: sawClose = True
        #next i,tokDef

        # Put delimiters and operands in the right places
        # Operands at the front
        if not sawClose:
            thisexpr = self._makeTokenExpr(TokName.CLOSE, ")")
            toks.insert(0,thisexpr)
        if not sawOpen:
            thisexpr = self._makeTokenExpr(TokName.OPEN, "(")
            toks.insert(0,thisexpr)

        # Done for sure at the end of the string.
        # Check it first every time.
        thisexpr = self._makeTokenExpr(self._tnEOI, StringEnd())
        toks.insert(0, thisexpr)

        # Check for operands after all other tokens.
        thisexpr = self._makeTokenExpr(self._tnOPERAND, operandExpr)
        toks.append(thisexpr)

        # And a catch-all.  If we see a token that can't be a valid operator,
        # we're done.  This is so BNF like
        #   statement ::= <lvalue> ':=' <expr> ';'
        # will work.  When the tokenizer hits the semicolon, it
        # needs to finish the expression cleanly so the next ParseElement
        # can munch the semicolon.
        thisexpr = self._makeTokenExpr(self._tnEOI, Empty())
        toks.append(thisexpr)

        self._tokenizer = MatchFirst(toks)
        if self.debug:
            self._tokenizer.setDebug()

        # Process the precedence tree. =====================================

        # For now, just checking associativity.
        # self._assocL holds whether operators are left-associative,
        # indexed by precName.
        self._assocL = {}  # True for left-assoc, False for right-assoc
        self._arity = {}   # Integer arity

        # Process the operators in two passes: an information-gathering and
        # error-checking pass, and a table-loading pass.
        # Some code is duplicated between Pass 1 and Pass 2, but I think the
        # duplication is worth it to keep Pass 2 cleaner.

        # -------------------------------------
        # Pass 1: Look for Adj.  If we have an Adj, we need to special-case it.
        # While we're at it, run some sanity checks on the input.

        self._hasAdjOp = False     # Whether this grammar has ADJ
        adj_rowidx = None           # Which precedence row the ADJ is on

        optoks = {}                 # context => which tokens we've seen.
        optoks[self._ctxPRE]=[]    # Each PRE or OP0 token can only be on one
        optoks[self._ctxOP0]=[]    # precedence row.

        for precrowidx, operDef in enumerate(opList):
            toknames, numtoknames, numTerms, RLAssoc, opParseAction = \
                    self._pullrow(precrowidx, operDef)

            # Sanity checks
            if numTerms<1:
                raise ValueError(
                    ("Invalid arity {} in row {} (starting with token {}) - "+
                     "must be >= 1").format(numTerms, precrowidx, toknames[0])
                )
            if (numTerms>2 and numtoknames!=numTerms-1):
                raise ValueError(("Wrong number of tokens ({}) for "+
                    "operator of arity {} (row {})").format(numtoknames,
                        numTerms, precrowidx))

            if TokName.OPEN in toknames or TokName.CLOSE in toknames:
                raise ValueError(
                    "Can't specify delimeters as operators (row {})".format(
                        precrowidx
                    ))

            # See if we have a valid ADJ (presently must be infix binary).
            if TokName.ADJ in toknames:
                if self._hasAdjOp:
                    raise ValueError(
                            "Can't specify two Adj operators (rows "+
                            "{}, {})".format(adj_rowidx, precrowidx))
                elif numtoknames!=1 or numTerms!=2:
                    raise ValueError("Can only use Adj as a binary operator"+
                                     "(row {})".format(precrowidx))
                else:   # numTerms==2 and numtoknames==1: valid Adj
                    self._hasAdjOp = True
                    adj_rowidx = precrowidx     # Save where it is
            # endif ADJ processing

            # Check for prohibited use of the same token in the same context
            # for two operators
            if numTerms==1:
                ctx = ( self._ctxPRE if (RLAssoc is Infix.PREFIX) 
                                        else self._ctxOP0 )
            else:   # numTerms>1: only check in OP0 context, because once we
                ctx = self._ctxOP0     # see that, we know what to expect.
            #endif

            therange = range(1 if numTerms>2 else numtoknames)
            # If we have a ternary or higher operator, only check the first
            # token since all the subsequent tokens are determined by the
            # first one.  Otherwise, we have multiple choices of token for
            # this operator, so check all of them.

            for toknidx in therange:
                if toknames[toknidx] in optoks[ctx]:
                    raise ValueError(
                        "Can't list token {} in two {} contexts".format(
                            toknames[toknidx], self._c2s(ctx)))

                optoks[ctx].append(toknames[toknidx])
            #next toknidx

        # next precrowidx

        # -------------------------------------
        # Pass 2: stuff arrays.  Remember that sanity checks were done 
        # in the loop above.

        for precrowidx, operDef in enumerate(opList):
            toknames, numtoknames, numTerms, RLAssoc, opParseAction = \
                    self._pullrow(precrowidx, operDef)

            # Unary -----------------------
            if numTerms==1:
                ctx = ( self._ctxPRE if (RLAssoc is Infix.PREFIX) 
                                        else self._ctxOP0 )
                for toknidx, tokname in enumerate(toknames):
                    opname = NamedConstant('unary{}.{}.{}.{}'.format(
                                                precrowidx,toknidx,tokname,ctx))
                    self._loadoperator(tokname, ctx, opname, precrowidx,
                                        RLAssoc, numTerms, adj_rowidx,
                                        opParseAction)
                #next tokname

            # Non-unary--------------------
            elif numTerms==2:
                for toknidx, tokname in enumerate(toknames):
                    if tokname is TokName.ADJ:
                        self._AdjOpExpr = self._makeTokenExpr(
                                                    TokName.ADJ, Empty())
                        self._AdjOpExpr.addParseAction(_stuffAdjOp)
                            # A ParseExpr that synthesizes adj ops.
                        opname = self._onADJ
                    else:
                        opname = NamedConstant( 'op{}.{}.{}.{}'.format(
                                                    precrowidx, toknidx,
                                                    tokname, ctx))
                            # Each occurrence of a token in the precedence list
                            # is a unique operator name.
                    #endif ADJ else

                    self._loadoperator(tokname, self._ctxOP0, opname, 
                                        precrowidx, RLAssoc, numTerms, 
                                        adj_rowidx, opParseAction)
                # next tokname

            else:   # numTerms >= 3
                # For now, only key off the first token.  For multi-arity
                # operators, all the remaining tokens are just checked for
                # a match.
                tokname = toknames[0]   # _pullrow guarantees this exists.

                if tokname is TokName.ADJ:
                    self._AdjOpExpr = self._makeTokenExpr(
                                                TokName.ADJ, Empty())
                    self._AdjOpExpr.addParseAction(_stuffAdjOp)
                        # A ParseExpr that synthesizes adj ops.
                    opname = self._onADJ
                else:
                    opname = NamedConstant( 'op{}.0.{}.{}'.format(
                                                precrowidx, tokname, ctx))
                        # 0 = index in toknames, for consistency with binops
                #endif ADJ else

                self._subops[opname]=[]    # Get ready for the loop below
                firstopname=opname

                self._loadoperator(tokname, self._ctxOP0, opname, 
                                    precrowidx, RLAssoc, numTerms, 
                                    adj_rowidx, opParseAction)

                # Store the remaining tokens we expect.  At present,
                # ADJ is not allowed in this list.
                for tokidx in range(1,numtoknames):
                    tokname = toknames[tokidx]
                    ctx = self._ctxOP0 + tokidx
                    opname = NamedConstant( 'op{}.{}.{}.{}'.format(
                                                precrowidx,tokidx,tokname,ctx))

                    # Save the mapping between first tokens and subsequent
                    # tokens for multi-arity operators.
                    self._subops[firstopname].append(opname)
                    self._loadoperator(tokname, ctx, opname, precrowidx)
                        # We don't use RLAssoc or numTerms, and this can't
                        # be an adj or a prefix op.
                # next tokidx
            #endif unary else
        # next in opList

        # Load precedences for operands and grouping
        self._prec[self._onOPEN] = self._precDELIM
        self._prec[self._onCLOSE] = self._precDELIM
            # at the bottom so it will drop us out of an operator loop
        self._prec[self._onOPERAND] = self._precOPERAND
        self._prec[self._onEOI] = self._precDONE
        self._prec[self._onMISPLACED] = self._precDONE
            # so misplaced operators will drop us out of precedence loops

    # end __init__

    # Init helpers
    def _pullrow(self, precrowidx, operDef):
        """ Pull the components of row operDef; regularize toknames into an 
            iterable and count the items.
            Returns toknames, numtoknames, numTerms, RLAssoc, opParseAction.
            Postcondition: numtoknames >= 1; toknames[0] exists."""
        try:
            toknames, numTerms, RLAssoc, opParseAction = \
                    (operDef + (None,))[:4]
                # None and :4 handle optional opParseAction.
                # toknames can be a tuple or not.
        except:     # While we're at it, get this check out of the way
            raise ValueError(
                ("Need tokens, arity, associativity, and optional"+
                    "parse action in row {} of opList").format(precrowidx)
            )

        # Regularize toknames to a list of >=1 element
        if isinstance(toknames, basestring):
            toknames = (toknames,)  # since strings are iterable
        try:
            numtoknames = len(toknames)
            if numtoknames<1:
                raise ValueError(
                        'Missing tokname on row {}'.format(precrowidx))
        except: #toknames wasn't iterable - wrap it in a tuple
            numtoknames=1
            toknames = (toknames,)

        # Enforce single parse actions, not a list of actions.
        try:
            palen = len(opParseAction)
            if palen>1:
                raise ValueError(
                    'More than one opParseAction specified on row {}'.format(
                        precrowidx))
            opParseAction = opParseAction[0]    # extract it from the list
        except:     # opParseAction isn't iterable, so it must be a single item.
            pass

        return toknames, numtoknames, numTerms, RLAssoc, opParseAction
    #end _pullrow

    def _loadoperator(self, tokName, ctx, opname, precrowidx,
                    RLAssoc=None, numTerms=None, adj_rowidx=None,
                    opParseAction=None):
        """ Load an operator into the tables.  Also checks for context
            collisions."""
            # OLD Precondition: if adj_rowidx is None, 
            # ctx is not self._ctxPRE.  As of 2013/05/17, I can't remember
            # why I added this precondition.
        
        HASPRE = "haspre"       # private constants
        HASPOST = "haspost"

        # Verify and load the mapping from tokname+ctx to opname --------

        # Have we seen this token before?
        if tokName not in self._tn2on:
            thismap = self._tn2on[tokName]={}
            # In which contexts has this token already appeared?
            thismap[HASPRE] = False
            thismap[HASPOST] = False
        else:
            thismap = self._tn2on[tokName]
        #endif new token else

        # Have we seen it in a conflicting context?
        if ctx>=self._ctxOP0 and thismap[HASPOST]:
            raise ValueError(("Operator re-use conflict: attempt to respecify"+
                            " post-operand operator token {} on row {}."+
                            "  This may mean you are using prefix and postfix"+
                            " forms of an operator in a grammar that also "+
                            "has an ADJ operator.").format(
                                tokName, precrowidx))

        elif ctx==self._ctxPRE and thismap[HASPRE]:
            raise ValueError(("Operator re-use conflict: attempt to respecify"+
                            " pre-operand operator token {} on row {}."+
                            "  This may mean you are using prefix and postfix"+
                            " forms of an operator in a grammar that also "+
                            "has an ADJ operator.").format(
                                tokName, precrowidx))
        #endif context checks

        # We haven't seen it in a conflicting context, so add it.
        thismap[ctx]=opname
        if opParseAction is not None:
            self._opParseActions[opname] = opParseAction

        # Update the context info
        if ctx==self._ctxPRE:
            thismap[HASPRE] = True
        else:   # anywhere after an operand counts as post
            thismap[HASPOST] = True
            # For now, a token can't be used, e.g., both as a binary
            # operator and as part of a ternary operator.  
        # endif update context info

        # Store relevant information for this operator ------------------

        # Store the precedences
        self._prec[opname] = self._maxoperatorprec - precrowidx
            # higher prec values come first in the list

        if RLAssoc is not None:
            self._assocL[opname] = RLAssoc is Infix.LEFT
            # Infix.LEFT is also Infix.POSTFIX
        if numTerms is not None:
            self._arity[opname] = numTerms

        # SPECIAL CASE for ADJ ------------------------------------------
        # If we have ADJ, a unary prefix operator with a precedence higher than
        # ADJ could appear in an OP0 context where we would otherwise expect
        # a unary-postfix or binary operator.  Therefore, load prefix operators
        # in the OP0 slots also.  This is only the case for operators with
        # precedences higher than ADJ because trying to drop down below
        # ADJ would be a syntax error.  E.g., for unary :: highest, then
        # binary +, binary ADJ, unary $,
        #       $3+2 5          is $((3+2) ADJ 5), but
        #       3+2 $5          is either a syntax error or two successive
        #                       exprs: (3+2) and ($5).
        # This does mean that you can either have a grammar with the same
        # token for prefix and postfix (e.g., ++ in C), OR you can have a
        # grammar with ADJ, but you can't have both.  This makes sense, because
        # with both,    3++5     could be either   (3++) 5   or   3 (++5)   .

        # I can't remember why this precondition existed. -2013/05/17
        #assert (adj_rowidx is not None) or (ctx!=self._ctxPRE)
        #    # Check precondition.

        if (self._hasAdjOp and     # hasadj implies adj_rowidx is not None
                numTerms==1 and RLAssoc is Infix.PREFIX and 
                precrowidx < adj_rowidx and
                ctx==self._ctxPRE):    # check ctx to avoid infinite
                                        # recursion on the next line.
            self._loadoperator(tokName, self._ctxOP0, opname, precrowidx, 
                            RLAssoc, numTerms, adj_rowidx)
                # This results in having a prefix operator and a postfix 
                # operator with the same prec.  This doesn't cause problems,
                # because you only see the one for the relevant context.
        # endif special case for adj

    # end _loadoperator

    # Tokenizer -------------------------------------------------------------

    # The parser runs one token behind the tokenizer.  loc, nexttoks
    # are where the tokenizer is, but nexttoks is the _next_ token to
    # be put in the return value or otherwise processed.
    # doneUpToLoc is the location where nexttoks *starts*; loc is where
    # nexttoks *ends*.
    # 1   +    1    *    2
    #               ^------- _loc=4
    #               ^------- _nexttoks=["*"]
    #           ^----------- _doneUpToLoc=3 - so far, 1+1 is on the
    #                        execution stack of _parse() to be returned.

    def _nexttoken(self, ctx):
        """ Call to advance to the next token.
            _ctx_ is the context in which the token should be interpreted."""

        if self._keeptoks:
            self._keeptoks = False     # and use same tokens.

        elif self._savedtoks is not None:
            # Location doesn't move if we're just putting back what we
            # had before.
            self._nexttoks = self._savedtoks
            self._savedtoks = None

        else:   # Normal call
            self._doneUpToLoc = self._loc     # save where this token started
            self._loc, self._nexttoks = \
                    self._tokenizer.parseImpl(self._str, self._loc, True)
            # Always do parse actions because we carry toknames in parseActions
        #endif saved else

        self._updatenext(ctx)

        if self.debug:
            theassoc = (
                ('Lassoc' if self._nextassocL else 'Rassoc') 
                if (self._nextarity>1) 
                else ('postfix' if self._nextassocL else 'prefix')
            )
            print('Token at {}, ctx {}: Got op {} ({}ary, {}, prec {})'.format(
                self._doneUpToLoc, ctx, self._nextopname, self._nextarity,
                theassoc, self._nextprec) )
        #endif debug

    # end _nexttoken

    def _ungettoks(self):
        """ Tell the tokenizer to rescan at the same point next time.
            Effectively, this ungets the last token."""
        self._keeptoks = True
    # end _ungettoks

    def _updatenext(self, ctx):
        """Grabs information about the current token so we don't have to
            keep indexing into maps in the parsing function."""

        tokname = self._nexttoks.get(self._taNAME)

        # Get the operator name for tokname in context ctx.
        # First, check special cases that can appear in any context.
        # (We don't have to special-case ADJ since that is in a contxt.)
        if tokname is TokName.OPEN:
            self._nextopname = self._onOPEN
        elif tokname is TokName.CLOSE:
            self._nextopname=self._onCLOSE
        elif tokname is self._tnEOI:
            self._nextopname=self._onEOI
        elif tokname is self._tnOPERAND:
            self._nextopname = self._onOPERAND
        else:
            try:
                self._nextopname = self._tn2on[tokname][ctx]
            except:
                self._nextopname = self._onMISPLACED
                    # Tell the caller there's a problem.
                    # Do not throw here because unexpected operators
                    # might be other tokens that will terminate an
                    # expression or subexpression.  MISPLACED has the
                    # lowest precedence, so it will terminate any loops
                    # looking for operators.
        # end grabbing opname

        # For convenience, pull some fields.  get() returns None instead
        # of throwing on missing keys.  Since we don't check assoc, arity,
        # or prec for open, close, operand, or EOI, leaving Nones in those
        # variables works fine.
        self._nextassocL = self._assocL.get(self._nextopname)
        self._nextarity = self._arity.get(self._nextopname, 0)
            # default arity is 0; we only test for ==1 or >1.
        self._nextoppa = self._opParseActions.get(self._nextopname)
            # opParseAction for this operator
        self._nextprec=self._prec.get(self._nextopname)

    # end _updatenext

    def _pushAdjOp(self):
        """Helper to push a fake Adj op into the token stream.
            Only used if the grammar supports it."""
        assert self._hasAdjOp
        self._savedtoks = self._nexttoks
        self._nexttoks = self._AdjOpExpr.parseString('')
            # Put a fake adj operator in nexttoks.  Use
            # loc that comes back.  parseString() rather than parseImpl
            # because parseImpl by itself doesn't run the parse actions,
            # and we need those.
        self._updatenext(self._ctxOP0)    # Right now, Adj can only be binop.
    # end _pushAdjOp()

    # Parser ----------------------------------------------------------------

    def parseImpl( self, instring, loc, doActions=True ):
        """Parse an expression.  returns loc, tokens.  NOT RE-ENTRANT."""

        # Init variables for this pass
        self._doneUpToLoc = -1 # How far we've gotten in parsing
        self._nexttoks=None    # Next token to be processed
        self._loc = loc        # How far the tokenizer has gotten.
        self._str = instring
        self._doActions = doActions
        self._keeptoks = False # if True, re-process the same tokens.

        self._savedtoks = None # Pushed-back toks for _nexttoken().

        retval = self._parse(self._precBOTTOM)    # Do it, Rockapella!

        if self._loc==loc:     # If we didn't munch anything
            raise ParseException(self._str, self._loc, 
                    "Expected an expression", self)

        return self._doneUpToLoc, (retval,)
            # doneUpToLoc because self._loc points to the first token _after_
            # the expression.
            # Wrap the results in a tuple because otherwise
            # (IELL+IELL) runs the results together.  E.g., 
            #   IELL.Parse("++5 ++6")
            # returns <++, 5, ++, 6> without the tuple.
            # With the tuple, it returns <(++,5), (++,6)>.
    # end parseImpl

    # --- The main worker function ---

    def _parse(self, currPrec):
        """Parse an expression beginning at currPrec.  Uses instance vars
            set by parseImpl().  Call _nexttoken() before calling this.
            Returns the resulting parse tree as a prefix nested list
            wrapped in a ParseResults instance."""
        # Variables are named for "operAND", "operaTOR".
        # Each call to _parse:
        #   1. grabs a (complex) operand with its prefix operators;
        #   2. grabs any postfix operators; and then
        #   3. processes any infix operators.
        # Therefore, whenever _parse is called, it expects
        # nexttoks to be an operand or complex operand (prefix/postfix/nested).

        # 1. Grab an operand. '''''''''''''''''''''''''''''''''''''''''''

        # This can be a simple operand, a parenthesized expression,
        # or either of those with any number of prefix operators,
        # as permitted by the syntax.  Postfix operators are handled in #2.
        # The operand is placed in var "retval".

        self._nexttoken(self._ctxPRE)  # Grab the next token.
            # The initial context is ctxPRE because we are before an operand.
            # We are thus looking for PREfix operators.
        if self.debug:
            print('Into parse at prec {}; saw {} after {}'.format(
                currPrec, self._nextopname, self._doneUpToLoc))

        # Check for nested exprs
        if self._nextopname is self._onOPEN:
            # The whole nested expr is the operand
            retval = self._parse(self._precBOTTOM)   # parse the nested expr
            self._nexttoken(self._ctxNONE)
            if not (self._nextopname is self._onCLOSE):
                raise ParseException(self._str, self._loc,
                        "Expected closing paren; got " + str(self._nexttoks),
                        self)
            retval = self._clean(retval)
            self._nexttoken(self._ctxOP0)
                # skip past the ')' to the operator.  We are after an operand,
                # so we are looking for postfix or binary operators => OP0.

        # Check for unary prefix operators of an appropriate precedence.
        # All unary prefix ops are right-associative.
        # The precedence check is because, to use a C++ example,
        # " *::foo " is syntactally valid but " ::*foo " is not.
        elif (  self._nextarity==1 and
                self._nextassocL == self._ASSOCL_PREFIX and
                self._nextprec >= currPrec):
            #print("Got {} prec {}".format(str(self._nexttoks),self._nextprec))
            # We have a prefix operator.  Save it.
            tortoks = self._nexttoks
            torprec = self._nextprec
            tortoks = self._clean(tortoks)

            # Get the expression that is the argument of the unary operator.
            retval = self._parse(torprec)
                # torprec rather than torprec+1 because right-associative.
                # This means, e.g., for prefix op "@", you can say " @@@<op> ".
            retval = self._clean(retval)

            # Now apply the unary operator to the arg.
            # Unary prefix operators get two-element result tuples.
            retval = ParseResults([tortoks, retval])

            # Now we have a full operand, so next should be postfix or binary.
            self._nexttoken(self._ctxOP0)

        # We didn't get a parenthesized expr, or a unary operator,
        # so we need an operand.
        elif self._nextopname is not self._onOPERAND:
            raise ParseException(self._str, self._loc,
                    "Expected operand; got " + str(self._nexttoks), self)

        else: # We got an operand.  Grab it.
            retval = self._nexttoks
            retval = self._clean(retval)
            self._nexttoken(self._ctxOP0)
                # The next token should be an operator or, at string end, EOI.
                # We are now post-operand, so OP0.
        #endif token-type checks up through the operand.

        # 2. Check for postfix operators. '''''''''''''''''''''''''''''''
        # E.g., for the grammar with postfix ops ^ and ++, ^ higher precedence,
        # foo^++ is syntactic but foo++^ is not.
        # Therefore, loop down much as we do for infix ops.
        if (self._nextarity==1 and
            self._nextassocL == self._ASSOCL_POSTFIX):
            for thisprec in range(self._nextprec, currPrec-1, -1):
                #pdb.set_trace() # DEBUG
                # Loop over all the postfix ops in this run.
                while ( self._nextarity==1 and
                        self._nextassocL == self._ASSOCL_POSTFIX and
                        self._nextprec==thisprec):
                    #print("Saw postfix {} at prec {}".format(
                    #        str(self._nexttoks), thisprec))
                    tortoks = self._nexttoks
                    tortoks = self._clean(tortoks)
                    retval = ParseResults([tortoks, retval, None])
                        # three-element form with [2]==None indicates postfix
                        # (as opposed to two-element unary form).
                        # I got this idea from C++ operator++()/operator++(int)
                        # and welcome better ideas for how to flag this.
                    self._nexttoken(self._ctxOP0)
                        # The next thing up can be another postfix, or
                        # an infix, so OP0
                # end while at this prec
            # end for postfix prec loop
        #endif saw a postfix op

        # 3. Process infix operators. '''''''''''''''''''''''''''''''''''

        # We now have one operand, possibly with unary operators.
        # Check for Adj operator, but only if the grammar permits it.
        if self._hasAdjOp and (
                self._nextopname is self._onOPERAND or
                self._nextopname is self._onOPEN or
                (   self._nextarity==1 and
                    self._nextassocL == self._ASSOCL_PREFIX)
        ):
            #print("Got an AdjOp")
            # We saw an operand, complex operand (unary prefix op), or
            # parenthesized expression right after
            # the operand we just grabbed.  E.g., "1 2", "1 (2)", "1 ++2".
            # Pretend there was a self._onADJ operator between them.
            # NOTE: This will catch prefix operators with precedences higher
            # than Adj because of the special case set up in initialization.
            self._pushAdjOp()  # sets _nexttoks to be Adj and queues up
                                # the operand or OPEN for after the Adj.
        #endif saw an Adj situation

        # Now check the precedences, looping from what we saw
        # down to the level we came in at.  This presently handles only
        # binary operations.
        rhstoks=[]
        for thisprec in range(self._nextprec, currPrec-1, -1):
            #print("At prec {}".format(thisprec))
            #pdb.set_trace() # DEBUG
            #print("toks {} arity {}".format(self._nextopname, self._nextarity))
            while (self._nextprec==thisprec):
                if self._nextarity==2:
                    tortoks = self._nexttoks
                    tortoks = self._clean(tortoks)
                    nextPrec = thisprec+1 if self._nextassocL else thisprec
                    rhstoks = self._parse(nextPrec)
                    rhstoks = self._clean(rhstoks)
                    retval = ParseResults([tortoks, retval, rhstoks])
                        # Make a tree of the results, in prefix form.
                    #print("toks {} arity {}".format(self._nextopname, self._nextarity))
                    self._nexttoken(self._ctxOP0) # look for next binop

                elif self._nextarity==3:
                    # We already have the first operand in retval.

                    # Handle operator 1, which is in ctx OP0
                    tor1toks = self._nexttoks   # e.g., '?'
                    tor1toks = self._clean(tor1toks)
                    nextPrec = thisprec+1 if self._nextassocL else thisprec
                    theop = self._nextopname

                    # The middle operand
                    midandtoks = self._parse(nextPrec)
                    midandtoks = self._clean(midandtoks)

                    # The second operator
                    self._nexttoken(self._ctxOP0+1)
                    if self._nextopname is not (self._subops[theop][0]):
                        raise ParseException(self._str, self._loc,
                            'Saw {} (op {}); expected op {} in '
                            'arity>=3 op'.format(
                                self._nexttoks, self._nextopname, 
                                self._subops[theop][0]))
                    tor2toks = self._nexttoks
                    tor2toks = self._clean(tor2toks)

                    # The right operand
                    rhstoks = self._parse(nextPrec)
                    rhstoks = self._clean(rhstoks)

                    # Put it together.
                    retval = ParseResults(
                        [(tor1toks, tor2toks), retval, midandtoks, rhstoks]
                    )

                    self._nexttoken(self._ctxOP0)
                        # We've swallowed the ternary, so next up should
                        # be a binary.

                elif self._nextarity==1:
                    # This catches, e.g., x#y for unary '#'.  If Adj is
                    # available and is lower precedence than #, though,
                    # x#y will become x Adj (#y).
                    raise ParseException(self._str, self._loc,
                        "Expected infix operator; got unary " + 
                            str(self._nexttoks), self)
                #endif arity switch

            # end while at this prec

        #end for each prec

        if self.debug:
            print("Leave parse after {}, prec {}".format(self._loc, currPrec))

        self._ungettoks()  # forget the tok that caused us to leave the
                            # parse so the next call can grab it.

        # Strip off nesting if there's only one element, e.g., on operands
        return self._stripifsingle(retval)

    # end _parse

    # Private helpers -------------------------------------------------------
    def _stripifsingle(self, item):
        retval = item
        try:
            rlen = len(retval)
            if rlen==1:
                retval = retval[0]
        except:
            pass
        return retval
    # end _stripifsingle

    def _elemfor(self, other, context):
        if isinstance( other, basestring ):
            return ParserElement.literalStringClass( other )
        elif isinstance( other, ParserElement ):
            return other.copy()
        else:
            raise ValueError(
                "Need a ParserElement or string for the {}".format(context))
    # end _elemfor

    def _makeTokenExpr(self, tokName, tokExpr):
        """Makes a ParserElement to check for tokExpr."""
        thisexpr = self._elemfor(tokExpr, "tokenizer")

        # Make a closure to carry precedence in.
        # Use default args to stay self-contained.  When I didn't use
        # default arguments, I had inappropriate precedences coming through.
        indict = {#self._taPREC: self._prec[precName],
                self._taNAME: tokName}
            # When you update this, also update self._clean() to remove
            # the keys listed here.

        def thisaction(s, l, thetoks, thedict=indict):
            # Must provide s and l args even though we don't use them,
            # because otherwise trim_arity swallows the _thedict_ arg and
            # this function never gets called.
            for k,v in thedict.items():
                thetoks[k]=v
                #print("    = Updated {} with {}=>{}".format(repr(thetoks),
                #    repr(k),repr(v)))
        #end thisaction()

        thisexpr.addParseAction(thisaction)
            # _add_ so it keeps any parse actions it may already have
            # carried over in the copy() of tokExpr
        if self.debug:
            thisexpr.setDebug()
        return thisexpr
    # end _makeTokenExpr

    def _clean(self, what):    # Remove internal-use fields from results
        #if self._taPREC in what: del what[self._taPREC]
        try:
            if self._taNAME in what: del what[self._taNAME]
        except:
            pass    # if we were trying to clean a string, don't worry
                    # that the "in" check failed.

        # Strip off nesting if there's only one element, e.g., on operands
        if not isinstance(what, basestring):
            # Strings are iterables, and we don't want to throw away the \
            # chars after the first.
            try:
                rlen = len(what)        # If it's an iterable, keep element 1.
                if rlen==1:
                    what = what[0]
            except:
                pass
        #endif not a string

        return what

    #end _clean

    # Other public functions ------------------------------------------------

    def copy(self):
        ret = super().copy()
        # Now ret is a shallow copy, so it includes all the constants.
        # Since the caller is required to treat this class as immutable,
        # we do not need to deep-copy any data.  However, do deep-copy the
        # matchers in case they have parseActions with state.
        # IELL._parse re-initializes its data when called.
        ret._operandExpr = self._operandExpr.copy()
        ret._tokenizer = self._tokenizer.copy()
        return ret
    # end copy()

# end class IELL / IELL

# Based on pyparsing.py
#
# Copyright (c) 2003-2015  Paul T. McGuire
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

